from rest_framework import viewsets
from .serializers import *
from .models import *


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.filter(is_exist=True)
    serializer_class = ClientSerializer

