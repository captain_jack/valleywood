from django.shortcuts import render


def home(request):
    return render(request, 'home.html', {"data": "Shohrux"})


def dashboard(request):
    return render(request, 'dashboard.html')


def clients(request):
    return render(request, 'valleywood/client/main.html')


def client_edit(request):
    return render(request, 'valleywood/client/edit.html')
