from django.contrib.auth.models import User
from django.db import models


class Client(models.Model):
    name = models.CharField(max_length=512, unique=True)
    phone = models.CharField(max_length=128, unique=True, null=True, blank=True)
    info = models.TextField(null=True, blank=True)
    cr_on = models.DateTimeField(auto_now_add=True)
    cr_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, related_name='client_cr_by')
    up_on = models.DateField(auto_now=True)
    up_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, related_name='client_up_by')
    is_exist = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'

    def __str__(self):
        return self.name


class App(models.Model):
    code = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=128)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class AppUser(models.Model):
    app = models.ForeignKey(App, null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return "{}-{}".format(self.app.code, self.user.username)


class Module(models.Model):
    app = models.ForeignKey(App, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=128, null=True)
    path = models.CharField(max_length=256, null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Role(models.Model):
    module = models.ForeignKey(Module, null=True, on_delete=models.SET_NULL)
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=128)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.code


class RoleUser(models.Model):
    role = models.ForeignKey(Role, null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return "{}-{}".format(self.role.code, self.user.username)


class Permission(models.Model):
    role = models.ForeignKey(Role, null=True, on_delete=models.SET_NULL)
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=128)
    active = models.BooleanField(default=True)


class UserPermission(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    permission = models.ForeignKey(Permission, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return "{}-{}".format(self.user.username, self.permission.code)


