from django.contrib import admin
from main.models import *


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass


@admin.register(App)
class AppAdmin(admin.ModelAdmin):
    pass


@admin.register(AppUser)
class AppUserAdmin(admin.ModelAdmin):
    pass


@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    pass


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    pass


@admin.register(RoleUser)
class RoleUserAdmin(admin.ModelAdmin):
    pass


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    pass


@admin.register(UserPermission)
class UserPermission(admin.ModelAdmin):
    pass

