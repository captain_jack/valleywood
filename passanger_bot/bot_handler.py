from django.shortcuts import render
import urllib.request
from django.shortcuts import render, HttpResponse
import telebot
from django.views.decorators.csrf import csrf_exempt
from passanger_bot.tokens import tokenlist
from passanger_bot import service
from passanger_bot.texts import text_prop
from passanger_bot.controller import InlineController
from passanger_bot.models import Vehicle, Driver, BotUser

token = tokenlist['regionaltaxibot']

bot = telebot.TeleBot(token)


@csrf_exempt
def index(request):
    if request.method == 'GET':
        return HttpResponse("Bot Url Page")
    if request.method == 'POST':
        bot.process_new_updates([
            telebot.types.Update.de_json(
                request.body.decode("utf-8")
            )
        ])
        return HttpResponse(status=200)


@bot.message_handler(commands=['start'])
def start(m):
    service.act_start(m.from_user.id, m.from_user.username, m.from_user.first_name, m.from_user.last_name)
    inlinehandlerstart = InlineController(m, m.from_user.id)
    inlinehandlerstart.lang_menu()


@bot.message_handler(content_types='text')
def message(m):
    userid = m.from_user.id
    step = int(service.get_step(m.from_user.id))
    lang = service.get_lang(userid)
    inlinehand = InlineController(m, m.from_user.id)
    if m.text == '⬅️Ortga' or m.text == '⬅️Назад':
        inlinehand.show_menu('main')
    elif m.text == 'Asosiy menu' or m.text == 'Главний меню':
        inlinehand.show_menu('main')





@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    userid = call.from_user.id
    inlinehandler = InlineController(m=call,
                                     userid=userid,
                                     messageid=call.message.message_id,
                                     call_data=call.data,
                                     chatid=call.message.chat.id)
    if call.data:
        key, val = call.data.split(':')
        if key == 'changelang':
            inlinehandler.lang_menu()
        elif key == 'setlang':
            service.set_lang(userid=userid, lang=val)
            inlinehandler.show_menu('main')
