from passanger_bot.models import *
import telebot
from telebot import types
from passanger_bot.texts import text_prop
from passanger_bot import service
from passanger_bot.tokens import tokenlist
import datetime

bot = telebot.TeleBot(tokenlist['regionaltaxibot'])


class InlineController:
    def __init__(self, m, userid, messageid=None, call_data=None, chatid=None):
        self.userid = userid
        self.messageid = messageid
        self.call_data = call_data
        self.chatid = chatid
        self.m = m

    def lang_menu(self):
        welcome = service.get_welcome()
        text = welcome.welcome_message
        photo = welcome.welcome_photo
        ikey = types.InlineKeyboardMarkup()
        ikey.add(types.InlineKeyboardButton(text="🇸🇱 O'zbek", callback_data='setlang:UZ'),
                 types.InlineKeyboardButton(text="🇷🇺 Русский", callback_data='setlang:RU'))
        if photo:
            bot.send_photo(self.userid, photo=open(photo.path, 'rb'))
        if self.chatid:
            bot.edit_message_text(chat_id=self.chatid, message_id=self.messageid, text=text,
                                  reply_markup=ikey,parse_mode='html')
        else:
            bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')

    def show_menu(self, menu_code):
        menu = Menu.objects.get(code=menu_code)
        buts = Button.objects.filter(menu=menu, isactive=True).order_by('-order')
        lang = service.get_lang(self.userid)
        text = {'UZ': menu.text_uz, 'RU': menu.text_ru}
        ikey = types.InlineKeyboardMarkup()
        if buts:
            for i in range(0, len(buts)-1, 2):
                label1 = {'UZ': buts[i].label_uz, 'RU': buts[i].label_ru}
                button1 = types.InlineKeyboardButton(text=label1[lang], callback_data=buts[i].callback)
                label2 = {'UZ': buts[i+1].label_uz, 'RU': buts[i+1].label_ru}
                button2 = types.InlineKeyboardButton(text=label2[lang], callback_data=buts[i+1].callback)
                ikey.add(button1, button2)
            if len(buts) % 2 != 0:
                labellast = {'UZ': buts.last().label_uz, 'RU': buts.last().label_ru}
                ikey.add(types.InlineKeyboardButton(text=labellast[lang], callback_data=buts.last().callback))
        if menu.photo:
            bot.send_photo(self.userid, photo=open(menu.photo.path, 'rb'))
            bot.send_message(self.userid, text=text[lang], reply_markup=ikey, parse_mode='html')
        elif self.chatid:
            bot.edit_message_text(chat_id=self.chatid, message_id=self.messageid,
                                  text=text[lang], reply_markup=ikey, parse_mode='html')
        else:
            bot.send_message(self.userid, text=text[lang], reply_markup=ikey, parse_mode='html')

    def send_menu(self, menu_code):
        menu = Menu.objects.get(code=menu_code)
        buts = Button.objects.filter(menu=menu, isactive=True).order_by('-order')
        lang = service.get_lang(self.userid)
        text = {'UZ': menu.text_uz, 'RU': menu.text_ru}
        ikey = types.InlineKeyboardMarkup()
        if buts:
            for i in range(0, len(buts) - 1, 2):
                label1 = {'UZ': buts[i].label_uz, 'RU': buts[i].label_ru}
                button1 = types.InlineKeyboardButton(text=label1[lang], callback_data=buts[i].callback)
                label2 = {'UZ': buts[i + 1].label_uz, 'RU': buts[i + 1].label_ru}
                button2 = types.InlineKeyboardButton(text=label2[lang], callback_data=buts[i + 1].callback)
                ikey.add(button1, button2)
            if len(buts) % 2 != 0:
                labellast = {'UZ': buts.last().label_uz, 'RU': buts.last().label_ru}
                ikey.add(types.InlineKeyboardButton(text=labellast[lang], callback_data=buts.last().callback))
        if menu.photo:
            bot.send_photo(self.userid, photo=open(menu.photo.path, 'rb'))
            bot.send_message(self.userid, text=text[lang], reply_markup=ikey, parse_mode='html')
        else:
            bot.send_message(self.userid, text=text[lang], reply_markup=ikey, parse_mode='html')

    def is_handler(self):
        key, val = self.call_data.split(':')
        if val == 'driver':
            if service.is_driver(self.userid):
                self.show_menu('driver')
            else:
                self.show_register_button()
                # lang = service.get_lang(self.userid)
                # self.ask(lang, text_prop['begindriverreg'][lang])
                # service.set_step(self.userid, 1)

    def show_register_button(self):
        ikey = types.InlineKeyboardMarkup()
        ikey.add(types.InlineKeyboardButton(text='Register',
                                            url=ALLOWED_HOSTS[0] + '/bot/driver-reg/' + str(self.userid)))
        bot.send_message(self.userid, text='Reg buttonni bosing', reply_markup=ikey, parse_mode='html')

    def ask(self, lang, text):
        ikey = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        ikey.add(text_prop['backbtn'][lang])
        ikey.add(text_prop['backmain'][lang])
        bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')

    def send_cars(self):
        cars = Vehicle.objects.all()
        if cars:
            lang = service.get_lang(self.userid)
            ikey = types.InlineKeyboardMarkup()
            for i in range(0, len(cars)-1, 2):
                label1 = cars[i].car_model
                button1 = types.InlineKeyboardButton(text=label1, callback_data='selectcar:' + str(cars[i].id))
                label2 = cars[i+1].car_model
                button2 = types.InlineKeyboardButton(text=label2, callback_data='selectcar:' + str(cars[i+1].id))
                ikey.add(button1, button2)
            if len(cars) % 2 != 0:
                labellast = cars.last().car_model
                ikey.add(types.InlineKeyboardButton(text=labellast, callback_data='selectcar:' + str(cars.last().id)))
            backbtn = types.InlineKeyboardButton(text=text_prop['backbtn'][lang], callback_data='back:registerdriver')
            backmainbtn = types.InlineKeyboardButton(text=text_prop['backmain'][lang], callback_data='menu:main')
            ikey.add(backbtn, backmainbtn)
            bot.send_message(self.userid, text=text_prop['selectcar'][lang], reply_markup=ikey, parse_mode='html')

    def select_car(self):
        lang = service.get_lang(self.userid)
        key, val = self.call_data.split(':')
        driver = Driver.objects.get(user__userid=self.userid)
        driver.car_id = val
        driver.save()
        bot.delete_message(self.chatid, self.messageid)
        self.ask(lang, text_prop['insertplatenum'][lang])
        service.set_step(self.userid, 2)

    def send_driver_confirmation(self):
        driver = Driver.objects.get(user__userid=self.userid)
        lang = service.get_lang(self.userid)
        if driver.photo:
            text = '<a href="' + str(driver.get_absolute_url()) + '">&#160</a> \n ' \
                   'Name: ' + str(driver.full_name) + '\n' \
                   'Car: ' + str(driver.car) + '\n' \
                   'Car num: ' + str(driver.license_plate) + '\n' \
                   'Malumotlar to`g`riligini tekshiring. To`gf`ri bo`lsa tasdiqlang. Xatolar bo`lsa ' \
                   'yoki o`zgartirmoqchi bo`lsangiz o`chirib qayta reg qiling.'
            ikey = types.InlineKeyboardMarkup()
            ikey.add(types.InlineKeyboardButton(text=text_prop['confirm'][lang], callback_data='confirmdriver:' + str(self.userid)))
            ikey.add(types.InlineKeyboardButton(text=text_prop['delete'][lang], callback_data='deletedriver:' + str(self.userid)))
            bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')
        else:
            bot.send_message(text=text_prop['photonotloaded'][lang], parse_mode='html')

    def confirm_driver(self):
        Driver.objects.filter(user__userid=self.userid).update(isactive=True)
        bot.delete_message(chat_id=self.chatid, message_id=self.messageid)
        self.send_menu('driver')

    def delete_driver(self):
        Driver.objects.filter(user__userid=self.userid).update(isactive=False)
        service.set_step(userid=self.userid, step=0)
        bot.delete_message(chat_id=self.chatid, message_id=self.messageid)
        self.send_menu('main')

    def back_handler(self):
        key, val = self.call_data.split(':')
        if val == 'main':
            service.set_step(self.userid, 0)
            self.show_menu('main')
        elif val == 'driver':
            self.show_menu('driver')

    def ask_phone(self):
        lang = service.get_lang(self.userid)
        ikey = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        ikey.add(types.KeyboardButton(text=text_prop['sendcontact'][lang], request_contact=True))
        ikey.add(text_prop['backbtn'][lang])
        bot.send_message(self.userid,
                         text='Sizda active elon yo`q. '
                                           'Elon qo`shish uchun kerakli ma`lumotlarni so`ralgan tartibda kriting. '
                                           'Telfon raqamingizni yuboring yoki +998XXXXXXXXX korinishda kiriting',
                         reply_markup=ikey)

    def ask_source(self):
        lang = service.get_lang(self.userid)
        regions = service.get_source_regions()
        ikey = types.InlineKeyboardMarkup()
        text = 'Регион отправления'
        for r in regions:
            ikey.add(types.InlineKeyboardButton(text=str(r.region_name_uz), callback_data='source:' + str(r.id)))
        ikey.add(types.InlineKeyboardButton(text=text_prop['backbtn'][lang], callback_data='back:driver'))
        bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')

    def ask_destination(self):
        lang = service.get_lang(self.userid)
        user = service.get_user(self.userid)
        regions = service.get_dest_regions(user.temp)
        ikey = types.InlineKeyboardMarkup()
        text = 'Регион назначения'
        for r in regions:
            ikey.add(types.InlineKeyboardButton(text=str(r.region_name_uz), callback_data='destination:' + str(r.id)))
        ikey.add(types.InlineKeyboardButton(text=text_prop['backbtn'][lang], callback_data='back:driver'))
        if self.chatid:
            bot.edit_message_text(chat_id=self.chatid, message_id=self.messageid,
                              text=text, reply_markup=ikey, parse_mode='html')
        else:
            bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')

    def ask_ride_date(self):
        lang = service.get_lang(self.userid)
        ikey = types.InlineKeyboardMarkup()
        text = 'Дата выезда'
        curr_date = datetime.datetime.now()
        for i in range(5):
            ikey.add(types.InlineKeyboardButton(text=curr_date.strftime('%d.%m.%Y'), callback_data='date:' + curr_date.strftime('%d.%m.%Y')))
            curr_date += datetime.timedelta(days=1)
        ikey.add(types.InlineKeyboardButton(text=text_prop['backbtn'][lang], callback_data='back:driver'))
        if self.chatid:
            bot.edit_message_text(chat_id=self.chatid, message_id=self.messageid,
                              text=text, reply_markup=ikey, parse_mode='html')
        else:
            bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')

    def ask_price(self):
        lang = service.get_lang(self.userid)
        ikey = types.InlineKeyboardMarkup()
        ikey.add(types.InlineKeyboardButton(text=text_prop['backbtn'][lang], callback_data='back:driver'))
        text='Qanchadan opketas ortacha? \n Narxni raqamlarda kiriting'
        if self.chatid:
            bot.edit_message_text(chat_id=self.chatid, message_id=self.messageid,
                              text=text, reply_markup=ikey, parse_mode='html')
        else:
            bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')

    def ask_delivery(self):
        lang = service.get_lang(self.userid)
        ikey = types.InlineKeyboardMarkup()
        text = 'Pochta olasmi?'
        ikey.add(types.InlineKeyboardButton(text='Ha', callback_data='delivery:1'))
        ikey.add(types.InlineKeyboardButton(text='Yo`q', callback_data='delivery:0'))
        ikey.add(types.InlineKeyboardButton(text=text_prop['backbtn'][lang], callback_data='back:driver'))
        if self.chatid:
            bot.edit_message_text(chat_id=self.chatid, message_id=self.messageid,
                              text=text, reply_markup=ikey, parse_mode='html')
        else:
            bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')

    def ask_available_sreats(self):
        lang = service.get_lang(self.userid)
        ikey = types.InlineKeyboardMarkup()
        text = 'Moshnezda nechta bo`sh joy bor ?'
        ride = service.get_unconfirmed_ride(userid=self.userid)
        seats = ride.driver.car.seats
        for i in range(1, int(seats)):
            ikey.add(types.InlineKeyboardButton(text=str(i), callback_data='seat:' + str(i)))
        ikey.add(types.InlineKeyboardButton(text=text_prop['backbtn'][lang], callback_data='back:driver'))
        if self.chatid:
            bot.edit_message_text(chat_id=self.chatid, message_id=self.messageid,
                              text=text, reply_markup=ikey, parse_mode='html')
        else:
            bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')

    def ask_comment(self):
        lang = service.get_lang(self.userid)
        ikey = types.InlineKeyboardMarkup()
        text = 'Elonga qo`shimcha malumotlar yozmoqchi bolsanigiz(qulayliklar, konditsioner ...) shu yerga kriting '
        ikey.add(types.InlineKeyboardButton(text=text_prop['backbtn'][lang], callback_data='back:driver'))
        if self.chatid:
            bot.edit_message_text(chat_id=self.chatid, message_id=self.messageid,
                              text=text, reply_markup=ikey, parse_mode='html')
        else:
            bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')

    def confirm_ride(self):
        lang = service.get_lang(self.userid)
        ikey = types.InlineKeyboardMarkup()
        ride = service.get_unconfirmed_ride(self.userid)
        text = 'E`loningizdagi ma`lumotlar to`g`riligini tekshring, to`g`ri bo`lsa tasdiqlang\n ' \
               'Haydovchi: ' + str(ride.driver.full_name) + '\n ' \
               'Tel: ' + str(ride.phone) + '\n' \
               'Qayerdan: ' + str(ride.source.region_name_uz) + '\n' \
               'Qayerga: ' + str(ride.destination.region_name_uz) + '\n' \
               'Jo`nab ketish sanasi: ' + ride.tdate.strftime("%d.%m.%Y") + '\n' \
               'Narxi: ' + str(ride.price) + '\n' \
               'Bo`sh o`rinlar: ' + str(ride.available_seats) + '\n' \
               'Pochta: ' + str(ride.isdelivery) + '\n' \
               'Comment: ' + str(ride.comment)
        ikey.add(types.InlineKeyboardButton(text='Tasdiqlash', callback_data='confirm_ride:' + str(ride.id)))
        ikey.add(types.InlineKeyboardButton(text=text_prop['backbtn'][lang], callback_data='back:driver'))



    def my_handler(self):
        lang = service.get_lang(self.userid)
        key, val = self.call_data.split(':')
        if val == 'profile':
            driver = Driver.objects.get(user__userid=self.userid)
            text = '<a href="' + str(driver.get_absolute_url()) + '">&#160</a> \n ' \
                   'Name: ' + str(driver.full_name) + '\n' \
                   'Car: ' + str(driver.car) + '\n' \
                   'Car num: ' + str(driver.license_plate) + '\n' \
                   'Is Active: ' + str(driver.isactive)
            ikey = types.InlineKeyboardMarkup()
            ikey.add(types.InlineKeyboardButton(text=text_prop['delete'][lang], callback_data='deletedriver:' + str(self.userid)))
            ikey.add(types.InlineKeyboardButton(text=text_prop['backbtn'][lang], callback_data='back:driver'))
            if self.chatid:
                bot.edit_message_text(chat_id=self.chatid, message_id=self.messageid,
                                      text=text, reply_markup=ikey, parse_mode='html')
            else:
                bot.send_message(self.userid, text=text, reply_markup=ikey, parse_mode='html')

        if val == 'ride':
            if service.is_ride_exist(self.userid):
                print('f')
            else:
                self.ask_phone()
                service.set_step(self.userid, 10)






















