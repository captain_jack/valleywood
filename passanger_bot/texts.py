text_prop = {
    'sharemylocation': {'UZ': 'Geolokatsiyani yuborish', 'RU': 'Отправить локацию'},
    'backbtn': {'UZ': '⬅️Ortga', 'RU': '⬅️Назад'},
    'backmain': {'UZ': 'Asosiy menu', 'RU': 'Главний меню'},
    'begindriverreg':
        {'UZ': 'Haydovchi bo`lib registratisyadan o`ting. Ism familyangizni to`liq kiriting ',
         'RU': 'Vedite vashe imya i familiya'},
    'selectcar': {'UZ': 'Mashinangiz rusumi qanday', 'RU': 'Модель вашего автомобиля'},
    'insertplatenum': {'UZ': 'Avtomobil davlat raqamini kiriting', 'RU': 'Введите гос номер вашего автомобиля'},
    'platenumexist':{'UZ': 'Raqam registratsiyadan o`tkazilgan', 'RU': 'Номер загеристрован'},
    'sendcarphoto': {'UZ': 'Moshnani rasmini yuboring', 'RU': 'Otpravte fotografiyu avtomobilya'},
    'confirm': {'UZ': 'Tasdiqlash', 'RU': 'ПОДТВЕРДИТЬ'},
    'delete': {'UZ': 'O`chirish', 'RU': 'Удалить'},
    'photonotloaded': {'UZ': 'Rasmni yuklashda xatolik bo`ldi qaytadan rasm yuboring',
                       'RU': 'Oshibka voznik zagruzkoy foto, otprvte foto yeshyoraz'},
    'sendcontact': {'UZ': 'Raqamni yuborish', 'RU': 'Отпрвить контакт'}

}
