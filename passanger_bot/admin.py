from django.contrib import admin
from passanger_bot.models import *


@admin.register(BotUser)
class BotUserAdmin(admin.ModelAdmin):
    list_display = ('userid', 'username', 'firstname', 'lastname', 'phone', 'join_on')
    search_fields = ('userid', 'username', 'firstname', 'lastname', 'phone')


@admin.register(Vehicle)
class VehicleAdmin(admin.ModelAdmin):
    list_display = ('car_model', 'seats')


@admin.register(Driver)
class DriverAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'license_plate', 'car', 'balance', 'isactive')
    search_fields = ('full_name', 'license_plate')
    list_filter = ('car', 'isactive')


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    pass


class ButtonsInline(admin.TabularInline):
    model = Button


@admin.register(Menu)
class MenuAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'text_uz', 'text_ru')
    search_fields = ('name',)
    inlines = [ButtonsInline, ]


@admin.register(Button)
class ButtonAdmin(admin.ModelAdmin):
    list_display = ('label_uz', 'label_ru', 'callback', 'order', 'menu', 'isactive')
    search_fields = ('label_uz', 'label_ru', 'menu__name')
    list_filter = ('menu',)


@admin.register(Ride)
class RideAdmin(admin.ModelAdmin):
    list_display = ('driver', 'source', 'destination', 'tdate', 'price', 'available_seats', 'isactive')
    search_fields = ('driver__license_plate', 'driver__full_name')
    list_filter = ('driver', 'driver__car', 'source', 'destination')


@admin.register(Welcome)
class WelcomeAdmin(admin.ModelAdmin):
    pass
