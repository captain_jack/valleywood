from django.db import models
from ValleyWood.settings import ALLOWED_HOSTS


class BotUser(models.Model):
    userid = models.CharField(max_length=90, unique=True)
    username = models.CharField(max_length=200, blank=True, null=True)
    firstname = models.CharField(max_length=60, blank=True, null=True)
    lastname = models.CharField(max_length=60, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    temp = models.CharField(max_length=1024, blank=True, null=True)
    step = models.IntegerField(default=0)
    lang = models.CharField(max_length=2, default='UZ')
    isactive = models.BooleanField(default=True)
    join_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Пользовател бота"
        verbose_name_plural = "Пользователи бота"
        ordering = ['-join_on']

    def __str__(self):
        return self.userid


class Vehicle(models.Model):
    car_model = models.CharField(max_length=120)
    seats = models.IntegerField()

    class Meta:
        verbose_name = "Автомобил"
        verbose_name_plural = "Автомобили"

    def __str__(self):
        return self.car_model


class Driver(models.Model):
    user = models.ForeignKey(BotUser, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=180, null=True, blank=True)
    car = models.ForeignKey(Vehicle, on_delete=models.CASCADE, null=True, blank=True)
    license_plate = models.CharField(max_length=10, null=True, unique=True, blank=True)
    photo = models.ImageField(upload_to='cars', blank=True, null=True)
    balance = models.DecimalField(max_digits=12, decimal_places=2, default=0.00)
    isactive = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Водитель"
        verbose_name_plural = "Водители"

    def get_absolute_url(self):
        return 'https://' + ALLOWED_HOSTS[0] + '/media/' + str(self.photo) + '/'

    def __str__(self):
        return '{} | {}'.format(self.license_plate, self.full_name)


class Region(models.Model):
    region_name_uz = models.CharField(max_length=120)
    region_name_ru = models.CharField(max_length=120)

    class Meta:
        verbose_name = "Область"
        verbose_name_plural = "Области"

    def __str__(self):
        return self.region_name_uz


class Menu(models.Model):
    code = models.CharField(max_length=60, unique=True)
    name = models.CharField(max_length=128)
    text_uz = models.TextField()
    text_ru = models.TextField()
    photo = models.ImageField(upload_to='menu', blank=True, null=True)

    def get_absolute_url(self):
        return 'https://' + ALLOWED_HOSTS[1] + str(self.photo) + '/'

    def __str__(self):
        return '{} | {}'.format(self.name, self.code)


class Button(models.Model):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    label_uz = models.CharField(max_length=120)
    label_ru = models.CharField(max_length=120)
    callback = models.CharField(max_length=64)
    order = models.IntegerField()
    isactive = models.BooleanField(default=True)

    def __str__(self):
        return self.callback


class Ride(models.Model):
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE, null=True, blank=True)
    phone = models.CharField(max_length=17, null=True, blank=True)
    source = models.ForeignKey(Region, on_delete=models.CASCADE, related_name='source', null=True, blank=True)
    destination = models.ForeignKey(Region, on_delete=models.CASCADE, related_name='destination', null=True, blank=True)
    tdate = models.DateField(verbose_name='Дата езды', null=True, blank=True)
    ttime = models.TimeField(verbose_name='Время езды', null=True, blank=True)
    price = models.IntegerField(verbose_name='Цена', null=True, blank=True)
    available_seats = models.IntegerField(verbose_name='Cвободные места', default=0)
    ispicker = models.BooleanField(verbose_name='Может добраться до клиента', default=False)
    isdelivery = models.BooleanField(verbose_name='Доставка почтой', default=False)
    comment = models.TextField(verbose_name='Примечание', null=True, blank=True)
    isactive = models.BooleanField(verbose_name='Актив', default=False)
    iscompleted = models.BooleanField(verbose_name='Заполнен', default=False)

    class Meta:
        verbose_name = 'Езда'
        verbose_name_plural = 'Езды'

    def __str__(self):
        return self.driver.license_plate


class Welcome(models.Model):
    welcome_photo = models.ImageField(upload_to='welcome', blank=True, null=True)
    welcome_message = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.welcome_message
