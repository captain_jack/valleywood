from datetime import datetime

from django.core.files.base import ContentFile
from django.utils.baseconv import base64

from passanger_bot.models import *
from django.core.files import File


def act_start(userid, username=None, firstname=None, lastname=None):
    checked = BotUser.objects.filter(userid=userid).exists()
    if checked:
        get = BotUser.objects.get(userid=userid)
        get.username = username
        get.firstname = firstname
        get.lastname = lastname
        get.step = 0
        get.save()
    else:
        BotUser.objects.create(userid=userid,
                               firstname=firstname,
                               lastname=lastname,
                               username=username)


def get_welcome():
    return Welcome.objects.all().first()


#############   Driver   Registration Functions ##############################
def is_driver(userid):
    return Driver.objects.filter(user__userid=userid, isactive=True).exists()


def is_ride_exist(userid):
    return Ride.objects.filter(driver__user__userid=userid, isactive=True).exists()


def set_driver_name(userid, fullname):
    user = BotUser.objects.get(userid=userid)
    iscreated = Driver.objects.filter(user=user).exists()
    if not iscreated:
        Driver.objects.create(user=user,
                              full_name=fullname)
    else:
        driver = Driver.objects.get(user=user)
        driver.full_name = fullname
        driver.license_plate = None
        driver.save()


def set_car_num(userid, carnum):
    plate = str(carnum).upper()
    isexist = Driver.objects.filter(license_plate=plate).exists()
    if not isexist:
        Driver.objects.filter(user__userid=userid).update(license_plate=plate)
        set_step(userid, 3)
    return isexist


def save_driver_photo(userid, file, filename):
    driver = Driver.objects.get(user__userid=userid)
    with open(file[0], 'rb') as ofile:
        driver.photo.save(str(filename), File(ofile))


def save_driver_image(userid, path, file):
    driver = Driver.objects.get(user__userid=userid)
    content = ContentFile(file)
    driver.photo.save(path, content, save=True)
########## End of Driver Registration #########################################


################# RIDE REGISTRATION ##############################################

def set_ride_contact(userid, phone):
    driver = Driver.objects.get(user__userid=userid)
    ride = Ride.objects.filter(driver__user__userid=userid, isactive=False, iscompleted=False).exists()
    if not ride:
        Ride.objects.create(driver=driver,
                        phone=phone)

def set_ride_source(userid, source_id):
    source = Region.objects.get(id=source_id)
    ride = Ride.objects.get(driver__user__userid=userid, isactive=False, iscompleted=False)
    ride.source = source
    ride.save()
    set_temp(userid, source_id)

def set_ride_destination(userid, dest_id):
    dest = Region.objects.get(id=dest_id)
    ride = Ride.objects.get(driver__user__userid=userid, isactive=False, iscompleted=False)
    ride.destination = dest
    ride.save()

def set_ride_date(userid, strdate):
    ride = Ride.objects.get(driver__user__userid=userid, isactive=False, iscompleted=False)
    ride.tdate = datetime.strptime(strdate, "%d.%m.%Y")
    ride.save()

def set_ride_price(userid, price):
    ride = Ride.objects.get(driver__user__userid=userid, isactive=False, iscompleted=False)
    ride.price = price
    ride.save()

def set_ride_delivery(userid, delivery):
    ride = Ride.objects.get(driver__user__userid=userid, isactive=False, iscompleted=False)
    if int(delivery) == 0:
        ride.isdelivery = False
    else:
        ride.isdelivery = True
    ride.save()

def set_ride_seats(userid, seats):
    ride = Ride.objects.get(driver__user__userid=userid, isactive=False, iscompleted=False)
    ride.available_seats = int(seats)
    ride.save()

def set_ride_comment(userid, comment):
    ride = Ride.objects.get(driver__user__userid=userid, isactive=False, iscompleted=False)
    ride.comment = comment
    ride.save()

def get_unconfirmed_ride(userid):
    ride = Ride.objects.get(driver__user__userid=userid, isactive=False, iscompleted=False)
    return ride


def get_source_regions():
    return Region.objects.all()

def get_dest_regions(source_id):
    return Region.objects.all().exclude(id=source_id)



def get_user(user_id):
    return BotUser.objects.get(userid=user_id)


def get_lang(userid):
    lang = BotUser.objects.get(userid=userid).lang
    return str(lang)


def get_step(userid):
    return BotUser.objects.get(userid=userid).step


def get_temp(userid):
    return BotUser.objects.get(userid=userid).temp


def set_lang(userid, lang):
    BotUser.objects.filter(userid=userid).update(lang=lang)


def set_step(userid, step):
    BotUser.objects.filter(userid=userid).update(step=step)


def set_temp(userid, temp):
    BotUser.objects.filter(userid=userid).update(temp=temp)





