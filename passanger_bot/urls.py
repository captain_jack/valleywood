from django.urls import path, include
from passanger_bot import views
from passanger_bot import bot_handler

urlpatterns = [
    path('api/', bot_handler.index),
    path('driver-reg/<int:user_id>', views.driver_register)
]
