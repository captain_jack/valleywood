from django.apps import AppConfig


class PassangerBotConfig(AppConfig):
    name = 'passanger_bot'
