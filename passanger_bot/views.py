from django.shortcuts import render
import urllib.request
from django.shortcuts import render, HttpResponse
import telebot
from django.views.decorators.csrf import csrf_exempt
from passanger_bot.tokens import tokenlist
from passanger_bot import service
from passanger_bot.texts import text_prop
from passanger_bot.controller import InlineController
from passanger_bot.models import Vehicle, Driver, BotUser

token = tokenlist['regionaltaxibot']

bot = telebot.TeleBot(token)


@csrf_exempt
def index(request):
    if request.method == 'GET':
        return HttpResponse("Bot Url Page")
    if request.method == 'POST':
        bot.process_new_updates([
            telebot.types.Update.de_json(
                request.body.decode("utf-8")
            )
        ])
        return HttpResponse(status=200)


@bot.message_handler(commands=['start'])
def start(m):
    service.act_start(m.from_user.id, m.from_user.username, m.from_user.first_name, m.from_user.last_name)
    inlinehandlerstart = InlineController(m, m.from_user.id)
    inlinehandlerstart.lang_menu()


@bot.message_handler(content_types='text')
def message(m):
    userid = m.from_user.id
    step = int(service.get_step(m.from_user.id))
    lang = service.get_lang(userid)
    inlinehand = InlineController(m, m.from_user.id)
    if m.text == '⬅️Ortga' or m.text == '⬅️Назад':
        inlinehand.show_menu('main')
    elif m.text == 'Asosiy menu' or m.text == 'Главний меню':
        inlinehand.show_menu('main')

    else:
        if step == 1:
            service.set_driver_name(userid=userid, fullname=m.text)
            inlinehand.send_cars()
        elif step == 2:
            res = service.set_car_num(userid, m.text)
            if res:
                bot.send_message(userid, text=text_prop['platenumexist'][lang])
            else:
                inlinehand.ask(lang, text_prop['sendcarphoto'][lang])
        elif step == 10:
            service.set_ride_contact(userid, m)
            inlinehand.ask_source()
        elif step == 11:
            if m.text.isdigit():
                service.set_ride_price(userid, m.text)
                inlinehand.ask_delivery()

            else:
                bot.send_message(userid, "raqamlarda kririting")
        elif step == 12:
            service.set_ride_comment(userid, m.text)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    userid = call.from_user.id
    inlinehandler = InlineController(m=call,
                                     userid=userid,
                                     messageid=call.message.message_id,
                                     call_data=call.data,
                                     chatid=call.message.chat.id)
    if call.data:
        key, val = call.data.split(':')
        if key == 'changelang':
            inlinehandler.lang_menu()
        elif key == 'setlang':
            service.set_lang(userid=userid, lang=val)
            inlinehandler.show_menu('main')
        elif key == 'menu':
            inlinehandler.show_menu(val)
        elif key == 'is':
            inlinehandler.is_handler()
        elif key == 'selectcar':
            inlinehandler.select_car()
        elif key == 'confirmdriver':
            inlinehandler.confirm_driver()
        elif key == 'deletedriver':
            inlinehandler.delete_driver()
        elif key == 'back':
            inlinehandler.back_handler()
        elif key == 'my':
            inlinehandler.my_handler()
        elif key == 'source':
            service.set_ride_source(userid, val)
            inlinehandler.ask_destination()
        elif key == 'destination':
            service.set_ride_destination(userid, val)
            inlinehandler.ask_ride_date()
        elif key == 'date':
            service.set_ride_date(userid, val)
            inlinehandler.ask_price()
            service.set_step(userid, 11)
        elif key == 'delivery':
            service.set_ride_delivery(userid, val)
            inlinehandler.ask_available_sreats()
        elif key == 'seat':
            service.set_ride_seats(userid, val)
            inlinehandler.ask_comment()
            service.set_step(userid, 12)


@bot.message_handler(content_types=['document'])
def file_handler(m):
    userid = m.from_user.id
    lang = service.get_lang(userid)
    step = service.get_step(userid)
    handler = InlineController(m=m, userid=userid)
    filename = m.document.file_name

    if step == 3:
        file = bot.get_file(m.document.file_id)
        f = urllib.request.urlretrieve('https://api.telegram.org/file/bot' + token + '/' + str(file.file_path))
        service.save_driver_photo(userid, f, filename)
        handler.send_driver_confirmation()


@bot.message_handler(content_types=['photo'])
def photo_handler(m):
    userid = m.from_user.id
    step = service.get_step(userid)
    handler = InlineController(m=m, userid=userid)

    if step == 3:
        raw = m.photo[1].file_id
        path = raw + ".jpg"
        file_info = bot.get_file(raw)
        downloaded_file = bot.download_file(file_info.file_path)
        service.save_driver_image(userid, path, downloaded_file)
        handler.send_driver_confirmation()


@bot.message_handler(content_types=['contact'])
def contact_handler(m):
    userid = m.from_user.id
    phone = m.contact.phone_number
    messageid = m.message_id
    user = service.get_user(userid)
    handler = InlineController(m=m, userid=userid)
    if user.step == 10:
        service.set_ride_contact(userid, phone)
        handler.ask_source()


def driver_register(request, user_id):
    if request.method == 'POST':
        try:
            user = BotUser.objects.get(userid=request.POST['userid'])
            car = Vehicle.objects.get(id=request.POST['car'])
            Driver.objects.create(
                user=user,
                full_name=request.POST['full_name'],
                car=car,
                license_plate=request.POST['license_plate'],
                photo=request.FILES['photo'],
                isactive=True
            ).save()
            res='Registratsiyadan o\'tdingiz!!! Bot orqali davom eting'
        except Exception as e:
            res = 'Xatolik: ' + str(e)
        print(request.POST)
        return HttpResponse(res)
    else:
        cars = Vehicle.objects.all()
        return render(request, 'bot/driver_register.html',
                  {'user_id': user_id,
                   'cars': cars})


#STEPS:
  # 10 driver phone
  # 11 ride price
  # 12 ride comment







