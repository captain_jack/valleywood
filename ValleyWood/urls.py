from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as authview
from main import views as mainviews
from main import tableviews as tableview
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from rest_framework import routers
from passanger_bot import urls as bot_urls
from wag import urls as wag_urls
from wag import views as wagviews

router = routers.DefaultRouter()
router.register('client', viewset=tableview.ClientViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('bot/', include(bot_urls)),
    path('admin/', admin.site.urls),
    path('login/', authview.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', authview.LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('register/', wagviews.register, name='register'),
    path('', wagviews.glavniy),
    path('gallery', wagviews.gallery),
    path('gallery/<int:id>', wagviews.galleryfilter),
    path('dash/', mainviews.dashboard, name='dashboard'),
    path('client/', mainviews.clients, name='client'),
    path('client_edit/', mainviews.client_edit, name='client_edit'),
    path('wag/', include(wag_urls))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

