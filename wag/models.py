from django.contrib.auth.models import User
from django.db import models
from colorfield.fields import ColorField

# Create your models here.

my_choises = (
                ('one','start'),
                ('two', 'doing'),
                ('three', 'finish')
)


class Vendor(models.Model):
    name = models.CharField(max_length=256, unique=True)

    class Meta:
        verbose_name = 'Vendor'
        verbose_name_plural = 'Vendors'

    def __str__(self):
        return self.name


class Client(models.Model):
    name = models.CharField(max_length=512,)
    phone = models.CharField(max_length=128, unique=True,)
    address = models.TextField(null=True, blank=True)
    description = models.TextField(max_length=1024, )
    cr_on = models.DateTimeField(auto_now=True)
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE, default=1)

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'

    def __str__(self):
        return self.name


class Order(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    state = models.CharField(max_length=100, choices=my_choises, default='one')
    description = models.TextField()
    deadline = models.CharField(max_length=128, null=True, blank=True)
    cr_on = models.DateTimeField(auto_now_add=True)
    up_on = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

    def __str__(self):
        return self.client.name


class Type(models.Model):
    name = models.CharField(max_length=256)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Type'
        verbose_name_plural = 'Types'

    def __str__(self):
        return self.name


class Raw(models.Model):
    name = models.CharField(max_length=256)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Raw'
        verbose_name_plural = 'Raws'

    def __str__(self):
        return self.name


class Color(models.Model):
    name = models.CharField(max_length=256)
    code = ColorField(default='#FF0000')
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Color'
        verbose_name_plural = 'Colors'

    def __str__(self):
        return self.name


class Item(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    type = models.ForeignKey(Type, null=True, on_delete=models.SET_NULL)
    raw = models.ForeignKey(Raw, null=True, on_delete=models.SET_NULL)
    color = models.ForeignKey(Color, null=True, on_delete=models.SET_NULL)
    width = models.CharField(max_length=128)
    height = models.CharField(max_length=128)
    description = models.TextField()
    quantity = models.IntegerField(default=1)
    price = models.IntegerField()

    def __str__(self):
        return self.order.client.name


class VendorUser(models.Model):
    vendor = models.ForeignKey(Vendor, null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.vendor.name


class Category(models.Model):
    name = models.CharField(verbose_name='Kategoriya', max_length=128)

    def __str__(self):
        return self.name


class Photo(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.TextField(verbose_name='Rasm uchun text', max_length=500)
    image = models.ImageField(verbose_name='Rasm', upload_to='valleywood')

    def __str__(self):
        return self.name