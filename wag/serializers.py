from rest_framework import serializers
from .models import *


class ClientSerializer(serializers.ModelSerializer):
    vendor_name = serializers.CharField(source='vendor')

    class Meta:
        model = Client
        fields = ('id', 'name', 'phone', 'address', 'description', 'cr_on', 'vendor_name')


class VendorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vendor
        fields = ('id', 'name')


class OrderSerializer(serializers.ModelSerializer):
    client_name = serializers.CharField(source='client')

    class Meta:
        model = Order
        fields = ('id', 'client_name', 'state', 'description', 'deadline', 'cr_on', 'up_on')


class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = ('name', 'active')


class RawSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = ('name', 'active')


class ColorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = ('name', 'code', 'active')


class ItemSerializer(serializers.ModelSerializer):
    order_name = serializers.CharField(source='order')
    type_name = serializers.CharField(source='type')
    raw_name = serializers.CharField(source='raw')
    color_name = serializers.CharField(source='color')

    class Meta:
        model = Item
        fields = ('id', 'order_name', 'type_name', 'raw_name', 'color_name', 'width', 'height', 'description', 'quantity', 'price',)
