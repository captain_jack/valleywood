from django.urls import path, include
from wag import views as wag_views
from rest_framework import routers
from wag import tableviews as tableview

router = routers.DefaultRouter()
router.register('client', viewset=tableview.ClientViewSet),
router.register('order', viewset=tableview.OrderViewSet),
router.register('item', viewset=tableview.ItemViewSet),

urlpatterns = [
    path('api/', include(router.urls)),
    # path('api/getclient/', wag_views.get_client, name='getclient'),
    # path('api/addclient/', wag_views.add_client, name='addclient'),
    # path('api/updateclient/<int:pk>', wag_views.update_client, name='updateclient'),
    # path('api/deleteclient/<int:pk>', wag_views.delete_client, name='deleteclient'),
    path('client/', wag_views.client, name='client'),
    path('order/', wag_views.order, name='order'),
    path('order/<int:id>', wag_views.item, name='orderitem'),
    path('tools/', wag_views.tools, name='tools'),
    path('client_add/', wag_views.addclient, name='addclient'),
    path('client_edit/<int:id>', wag_views.editclient, name='addclient'),
    path('client_delete/<int:id>', wag_views.deleteclient, name='deleteclient'),
    path('order_edit/<int:id>', wag_views.editorder, name='addorder'),
    path('order_delete/<int:id>', wag_views.deleteorder, name='deleteorder'),
    path('client_addorder/', wag_views.addorder, name='addorder'),
    path('client_additem/', wag_views.additem, name='additem'),

]