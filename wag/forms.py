from django import forms
from .models import *


class ClientForms(forms.ModelForm):

    class Meta:
        model = Client
        fields = '__all__'


class OrderForms(forms.ModelForm):

    class Meta:
        model = Order
        fields = '__all__'


class ItemForms(forms.ModelForm):

    class Meta:
        model = Item
        fields = '__all__'


class TypeForms(forms.ModelForm):

    class Meta:
        model = Type
        fields = '__all__'


class RawForms(forms.ModelForm):
    class Meta:
        model = Raw
        fields = '__all__'


class ColorForms(forms.ModelForm):
    class Meta:
        model = Color
        fields = '__all__'


class VendorForms(forms.ModelForm):
    class Meta:
        model = Vendor
        fields = '__all__'
