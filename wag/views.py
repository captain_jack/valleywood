from django.shortcuts import render,redirect
import json
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import *
from .serializers import *
from wag.forms import ClientForms, OrderForms, ItemForms, TypeForms, RawForms, ColorForms
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from django.contrib.auth.models import User, auth


def client(request):
    return render(request, 'wag/client/main.html')


def glavniy(request):
    return render(request, 'glavniy.html')


def gallery(request):
    category = Category.objects.all()
    photos = Photo.objects.all()
    photos = {
        'photos': photos,
        'categories': category,
    }
    return render(request, 'gallery.html', photos)



def galleryfilter(request, id):
    categori = Category.objects.get(id=id)
    category = Category.objects.all()
    photos = Photo.objects.filter(category_id=categori)
    photos = {
        'photos': photos,
        'categories': category,
    }
    return render(request, 'gallery.html', photos)



def order(request):
    forms = OrderForms()
    return render(request, 'wag/client/main_order.html', {'forms': forms})


def item(request, id):
    order = Order.objects.get(id=id)
    item = Item.objects.filter(order=id)
    context = {
        'item': item,
        'order': order,
    }
    return render(request, 'wag/client/itemdetails.html', context)


def tools(request):
    types = TypeForms()
    raw = RawForms()
    color = ColorForms()
    context = {
        'type': types,
        'raw': raw,
        'color': color

        }
    if request.method == "POST":
        types = TypeForms(request.POST)
        raw = RawForms(request.POST)
        color = ColorForms(request.POST)
        if request.POST.get('form_type') == "formOne":
            types.save()
            return redirect('tools')
        if request.POST.get('form_type') == "formTwo":
            raw.save()
            return redirect('tools')
        if request.POST.get('form_type') == "formThree":
            color.save()
            return redirect('tools')

    return render(request, 'wag/client/tools.html', context)


def addclient(request):
    if request.method == 'POST':
        form = ClientForms(request.POST)
        if form.is_valid():
            form.save()
            return redirect('client')
    else:
        clients = ClientForms()
        context = {
            "forms": clients,
        }
        return render(request, 'wag/client/add.html', context)


def addorder(request):
    if request.method == 'POST' and request.POST.get('form_type') == 'formOne':
        if request.is_ajax():
            clients = Client.objects.get(id=request.POST['client'])
            orders = Order.objects.create(
                client=clients,
                deadline=request.POST['deadline'],
                description=request.POST['description']
            )
            orders.save()
            response = {
                'msg': 'Your form has been submitted successfully'  # response message
            }
            return JsonResponse(response)
    if request.method == 'POST' and request.POST.get('form_type') == 'formTwo':
        if request.is_ajax():
            print(request.POST)
            order_len = len(Order.objects.all())-1
            obj = Order.objects.all()[int(order_len)]
            types = Type.objects.get(id=request.POST['t'])
            raw = Raw.objects.get(id=request.POST['raw'])
            color = Color.objects.get(id=request.POST['color'])
            items = Item.objects.create(
                order=obj,
                type=types,
                raw=raw,
                color=color,
                width=request.POST['width'],
                height=request.POST['height'],
                quantity=request.POST['quantity'],
                price=request.POST['price'],
                description=request.POST['description2']
            )
            items.save()
            response = {
                'msg': 'Your form has been submitted successfully item '
            }
            return JsonResponse(response)
    else:
        clients = Client.objects.all()
        types = Type.objects.all()
        raws = Raw.objects.all()
        colors = Color.objects.all()
        context = {
            'clients': clients,
            'types': types,
            'raws': raws,
            'colors': colors,
        }
        return render(request, 'wag/client/addorder1.html', context)


def additem(request):
    form = ItemForms()
    return render(request, 'wag/client/additem.html', {'forms': form})


def editorder(request, id):
    orders = Order.objects.get(id=id)
    form = OrderForms(instance=orders)
    if request.method == "POST":
        form = OrderForms(request.POST, instance=orders)
        if form.is_valid():
            form.save()
            return redirect('/wag/order')
    return render(request, 'wag/client/addorder.html', {'forms': form})


def editclient(request, id):
    clients = Client.objects.get(id=id)
    form = ClientForms(instance=clients)
    if request.method == "POST":
        form = ClientForms(request.POST, instance=clients)
        if form.is_valid():
            form.save()
            return redirect('/wag/client')
    return render(request, 'wag/client/edit.html', {'forms': form})


def deleteclient(request, id):
    clients = Client.objects.get(id=id)
    clients.delete()
    return redirect('/wag/client')


def deleteorder(request, id):
    orders = Order.objects.get(id=id)
    orders.delete()
    return redirect('/wag/order')


def register(request):
    if request.method == "POST":
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        username = request.POST['username']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        email = request.POST['email']

        if password1 == password2:
            if User.objects.filter(username=username).exists():
                print('Username take')
            elif User.objects.filter(email=email).exists():
                print('Email take')
            else:
                user = User.objects.create_user(
                    username=username,
                    password=password1,
                    email=email,
                    first_name=firstname,
                    last_name=lastname,
                )
                user.save()
                print('User created')
        else:
            print('Password not matching..')
        return redirect('/')
    else:
        return render(request, 'register.html')


    # if request.method == "POST":
    #     items = Item.objects.create(
    #         order=Order.objects.get(id=request.POST['order']),
    #         type=Type.objects.get(id=request.POST['t']),
    #         raw=Raw.objects.get(id=request.POST['raw']),
    #         color=Color.objects.get(id=request.POST['color']),
    #         width=request.POST['width'],
    #         height=request.POST['height'],
    #         quantity=request.POST['quantity'],
    #         price=request.POST['price'],
    #         description=request.POST['description2']
    #     )
    #     items.save()
# vendor = Vendor.objects.get(id=request.POST['vendor'])
# clients = Client.objects.create(
#         name=request.POST['name'],
#         phone=request.POST['phone'],
#         address=request.POST['address'],
#         description=request.POST['description'],
#         vendor=vendor
# )
# clients.save()
# print('client created:' + request.POST)


# @api_view(["GET"])
# @csrf_exempt
# @permission_classes([IsAuthenticated])
# def welcome(request):
#     content = {"message": "Welcome to the BookStore!"}
#     return JsonResponse(content)
#
#
# @api_view(["GET"])
# @csrf_exempt
# @permission_classes([IsAuthenticated])
# def get_client(request):
#     data = Client.objects.filter()
#     serializer = ClientSerializer(data, many=True)
#     return JsonResponse({'data': serializer.data}, safe=False, status=status.HTTP_200_OK)
#
#
# @api_view(["POST"])
# @csrf_exempt
# @permission_classes([IsAuthenticated])
# def add_client(request):
#     serializer = ClientSerializer(data=request.data)
#     if serializer.is_valid():
#         serializer.save()
#         return Response(serializer.data, status=status.HTTP_201_CREATED)
#     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
# @api_view(["PUT"])
# @csrf_exempt
# @permission_classes([IsAuthenticated])
# def update_client(request, pk):
#     payload = json.loads(request.body)
#     try:
#         client_item = Client.objects.filter(id=pk)
#         # returns 1 or 0
#         client_item.update(**payload)
#         client = Client.objects.get(id=pk)
#         serializer = ClientSerializer(client)
#         return JsonResponse({'client': serializer.data}, safe=False, status=status.HTTP_200_OK)
#     except ObjectDoesNotExist as e:
#         return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
#     except Exception:
#         return JsonResponse({'error': 'Something terrible went wrong'}, safe=False,
#                                 status=status.HTTP_500_INTERNAL_SERVER_ERROR)
#
# @api_view(["DELETE"])
# @csrf_exempt
# @permission_classes([IsAuthenticated])
# def delete_client(request, pk):
#     try:
#         client = Client.objects.get(id=pk)
#         client.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)
#     except ObjectDoesNotExist as e:
#         return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
#     except Exception:
#         return JsonResponse({'error': 'Something went wrong'}, safe=False,
#                             status=status.HTTP_500_INTERNAL_SERVER_ERROR)